<?php require_once __DIR__.'/settings.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Приложение Структура компании</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="assets/styles/main.css">
</head>
<body>

	<div class='container-fluid' data-display='1'></div>
	<div class='message' data-notice='1'>Message</div>

	<script src="//api.bitrix24.com/api/v1/"></script>
	<script src='assets/scripts/main.js'></script>
	<!-- plugins -->
	<script src='assets/scripts/plugins/AjaxModule.js'></script>
	<script src='assets/scripts/plugins/RequireFileModule.js'></script>
	<script src='assets/scripts/plugins/Window.js'></script>
	<script src='assets/scripts/plugins/Notice.js'></script>
	<!-- actions -->
	<script src='assets/scripts/actionCurrentCompany.js'></script>
	<script>
		const USERS = <?=json_encode($USERS);?>;
		const element = new UnitStructure(<?=json_encode($CURRENT_COMPANY['result']);?>);
		element.render();
	</script>

</body>
</html>