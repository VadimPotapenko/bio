/**
* content string
* type    string
*/
APP.notice = function (options) {
	function showNotice() {
		let notice = document.querySelector('[data-notice]');
		notice.innerHTML = options.content;
		notice.classList.add(options.type);
		notice.style.display = 'block';
		setTimeout(() => notice.style.display = 'none', 3000);
	}
	showNotice();
}