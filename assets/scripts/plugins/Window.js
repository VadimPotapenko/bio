/**
* title         string
* content       string
* beforeClose   function
* beforeSuccess function
*/
APP.window = function (options) {
	const Mwindow = document.createElement('div');
	function _createWindow () {
		Mwindow.classList.add('window');
		Mwindow.innerHTML = `
			<div class='container'>
				<div class='row'>
					<div class='col mt-3'>
						<p class='text-center h4 text-dark'>${options.title}</p>
					</div>
				</div>

				<div class='row'>
					<div class='col mt-3 display-content'>${options.content}</div>
				</div>
				<div class='buttons'>
					<button class='wbtn success'>Сохранить</button>
					<button class='wbtn cancel'>Отменить</button>
				</div>
			</div>
		`;

		Mwindow.lastElementChild.lastElementChild.children[0].addEventListener('click', successClose);
		Mwindow.lastElementChild.lastElementChild.children[1].addEventListener('click', cancelClose);
		return Mwindow;
	}

	function cancelClose (e) {
		options.beforeClose();
		e.target.removeEventListener('click', cancelClose);
		Mwindow.remove();
	}

	function successClose (e) {
		options.beforeSuccess();
		e.target.removeEventListener('click', cancelClose);
		Mwindow.remove();
	}

	return _createWindow();
}