/*
* method    string
* data      obj
* formData  bool
* headers   obj
* callback  func
* url       string
* transport string
*/
APP.fetch = function (options) {
	getFetch()
	.then(answer => {options.callback(answer)})
	.catch(error => {console.log('UserError: ' + error)});

	async function getFetch () {
		let answer = 'Server is not available';
		const dataRes = {
			method: options.method,
			body: options.data
		}

		if (!options.formData && options.method.toUpperCase() !== 'GET') {
			dataRes.body = JSON.stringify(options.data);
			dataRes.headers = options.headers || {'Content-Type': 'application/json'};
		}

		let response = await fetch(options.url, dataRes);
		if (response.ok) answer = await response[options.transport]();
		return answer;
	}
}