/**
* type     string
* src      string
* id       string
* callback function
*/
APP.requireFile = function (options) {
	let element;
	switch (options.type) {
		case 'js':
			element = document.createElement('script');
			element.src = options.src;
			element.dataset.id = options.id;
			element.onload() = () => options.callback(element)
			document.head.append(element);
			break;

		case 'css':
			element = document.createElement('link');
			element.rel = 'stylesheet';
			element.type = 'text/css';
			element.href = options.src;
			element.dataset.id = options.id
			document.head.append(element);
			break;
	}
}

/**
* id string
*/
APP.removeFile = function (options) {
	[...document.head.children].forEach(value => {
		if (value.dataset.id == options.id) value.remove();
	});
}