class UnitStructure {
	constructor (data) {
		this.currentCompany = data;
		this.lab = {};
		this.storage = {};
		this.buh = {};
		this.pharmacy = {};
	}

	render() {
		this.element = document.createElement('div');
		this.element.classList.add('row');
		this.element.classList.add('pb-3');
		this.element.classList.add('mt-3');
		this.element.classList.add('border-bottom');

		let html = `
			<div class='col display' data-company-${this.currentCompany['ID']}>
				<div class='company-title'>
					<p class='h5 mt-3 text-dark'>${this.currentCompany['TITLE']}</p>
				</div>

				<div class='company-element' data-type='lab'>
					<p class='text-center h5 text-light mt-2'>Лаборатория</p>
					<p class='text-center h6 text-light mt-2'></p>
					<div class='contact' data-type='lab'>
						<button class='btn-contact btn' data-btn-${this.currentCompany['ID']}>Создать</button>
					</div>
					<span class='add-element' data-element-${this.currentCompany['ID']}></span>
				</div>

				<div class='company-element' data-type='storage'>
					<p class='text-center h5 text-light mt-2'>Склад</p>
					<p class='text-center h6 text-light mt-2'></p>
					<div class='contact' data-type='storage'>
						<button class='btn-contact btn' data-btn-${this.currentCompany['ID']}>Создать</button>
					</div>
					<span class='add-element' data-element-${this.currentCompany['ID']}></span>
				</div>

				<div class='company-element' data-type='buh'>
					<p class='text-center h5 text-light mt-2'>Бухгалтерия</p>
					<p class='text-center h6 text-light mt-2'></p>
					<div class='contact' data-type='buh'>
						<button class='btn-contact btn' data-btn-${this.currentCompany['ID']}>Создать</button>
					</div>
					<span class='add-element' data-element-${this.currentCompany['ID']}></span>
				</div>

				<div class='company-element' data-type='pharmacy'>
					<p class='text-center h5 text-light mt-2'>Аптека</p>
					<p class='text-center h6 text-light mt-2'></p>
					<div class='contact' data-type='pharmacy'>
						<button class='btn-contact btn' data-btn-${this.currentCompany['ID']}>Создать</button>
					</div>
					<span class='add-element' data-element-${this.currentCompany['ID']}></span>
				</div>
			</div>
		`;

		this.element.innerHTML = html;
		document.querySelector('[data-display]').append(this.element);
		this.init();
	}

	init() {
		let unit = this;
		[...document.querySelectorAll(`[data-btn-${this.currentCompany['ID']}]`)]
		.forEach(value => {
			value.addEventListener('click', function (e) {
				let type = e.target.parentNode.dataset.type;
				APP.requireFile({
					type: 'css',
					src: 'assets/styles/window.css',
					id: 'window'
				});

				APP.fetch({
					method:'POST',
					data:'company',
					url:'handlers/getFields.php',
					transport: 'json',
					callback(data) {
						let content = createWindow(data);
						document.body.append(APP.window({
							title:'Новая компания',
							content: content,
							beforeClose() {
								document.body.removeEventListener('click', finder);
								APP.removeFile({id:'window'});
							},
							beforeSuccess() {
								let formData = new FormData(document.forms[0]);
								APP.fetch({
									method:'POST',
									data: formData,
									formData: true,
									url: 'handlers/addCompany.php',
									transport: 'json',
									callback(data) {
										if (data) {
											unit.checkChild(data, 'company', type, 'data');
											APP.notice({
												content: `<p class="text-center h5 pt-2">ID ${data}. Создана новая компания.</p>`,
												type: 'm-success'
											});

										} else {
											APP.notice({
												content: '<p class="text-center h5 pt-2">Произошла ошибка. Компания не создалась.</p>',
												type: 'm-danger'
											});
										}
									}
								});
								document.body.removeEventListener('click', finder);
							}
						}));
					}
				});
				e.stopImmediatePropagation();
			});
		});

		[...document.querySelectorAll(`[data-element-${this.currentCompany['ID']}]`)]
		.forEach(value => {
			value.addEventListener('click', function (e) {
				if (unit[e.target.parentElement.dataset.type]['data']) {
					let element = new UnitStructure(unit[e.target.parentElement.dataset.type]['data']);
					element.render();
					let btn = document.createElement('span');
					btn.classList.add('new-element');
					btn.addEventListener('click', function (e) {
						console.dir(e);
					});

					value.parentElement.append(btn);
					value.remove();

				} else {
					APP.notice({
						content: '<p class="text-center h5 pt-2">Произошла ошибка. Компания не создана.</p>',
						type: 'm-danger'
					});
				}
				e.stopImmediatePropagation();
			});
		});
	}

	updateCard(type, key) {
		let unit = this;
		[...this.element.firstElementChild.children]
		.forEach(value => {
			if (value.dataset.type == type) {
				value.children[1].innerText = unit[type][key].TITLE;
				unit.changeButton(value, this);
				value.addEventListener('dblclick', function (e) {
					BX24.openApplication({
						'action':'widget_url',
						'url':  'https://' + BX24.getDomain() + `/crm/company/details/${unit[type][key].ID}/?IFRAME=Y&IFRAME_TYPE=SIDE_SLIDER`
					});
				});
			}
		});
	}

	checkChild(id, type, child, key) {
		let url = type == 'company' ? 'handlers/checkCompany.php' : 'handlers/checkContact.php'
		let unit = this;
		APP.fetch({
			method:'POST',
			data:id,
			url:url,
			transport: 'json',
			callback(data) {
				unit[child][key] = data;
				unit.updateCard(child, key);
			}
		});
	}

	changeButton(value, obj) {
		let parent = value.children[2];
		value.children[2].children[0].remove();

		let contactFace = document.createElement('span');
		contactFace.classList.add('btn-link');
		contactFace.innerText = 'Создать контактное лицо';
		contactFace.addEventListener('click', function () {
			let btns = this.parentElement;
			let type = btns.parentElement.dataset.type;
			APP.requireFile({
				type: 'css',
				src: 'assets/styles/window.css',
				id: 'window'
			});
			APP.fetch({
				method:'POST',
				data:'contact',
				url:'handlers/getFields.php',
				transport: 'json',
				callback(data) {
					let content = createWindow(data);
					document.body.append(APP.window({
						title:'Новая контакт',
						content: content,
						beforeClose() {
							document.body.removeEventListener('click', finder);
							APP.removeFile({id:'window'});
						},
						beforeSuccess() {
							let formData = new FormData(document.forms[0]);
							APP.fetch({
								method:'POST',
								data: formData,
								formData: true,
								url: 'handlers/addContact.php',
								transport: 'json',
								callback(data) {
									if (data) {
										APP.fetch({
											method:'POST',
											data:data,
											url:'handlers/checkContact.php',
											transport: 'json',
											callback(data) {
												obj[type]['contact'] = data;
												btns.children[0].remove();

												let contact = document.createElement('span');
												contact.classList.add('btn-link');
												contact.innerText = obj[type]['contact'].LAST_NAME + ' ' + obj[type]['contact'].NAME;
												contact.addEventListener('click', function () {
													BX24.openApplication({
														'action':'widget_url',
														'url':  'https://' + BX24.getDomain() + `/crm/contact/details/${obj[type]['contact'].ID}/?IFRAME=Y&IFRAME_TYPE=SIDE_SLIDER`
													});
												});
												btns.prepend(contact);
											}
										});

										APP.notice({
											content: `<p class="text-center h5 pt-2">ID ${data}. Создан новый контакт.</p>`,
											type: 'm-success'
										});

									} else {
										APP.notice({
											content: '<p class="text-center h5 pt-2">Произошла ошибка. Контакт не создался.</p>',
											type: 'm-danger'
										});
									}
								}
							});
							document.body.removeEventListener('click', finder);
						}
					}));
				}
			});
		});
		parent.append(contactFace);

		if (parent.dataset.type == 'lab') {
			let diler = document.createElement('span');
			diler.classList.add('btn-link');
			diler.innerText = 'Создать диллера';
			diler.addEventListener('click', function () {
				let btns = this.parentElement;
				APP.requireFile({
					type: 'css',
					src: 'assets/styles/window.css',
					id: 'window'
				});
				APP.fetch({
					method:'POST',
					data:'company',
					url:'handlers/getFields.php',
					transport: 'json',
					callback(data) {
						let content = createWindow(data);
						document.body.append(APP.window({
							title:'Новая компания',
							content: content,
							beforeClose() {
								document.body.removeEventListener('click', finder);
								APP.removeFile({id:'window'});
							},
							beforeSuccess() {
								let formData = new FormData(document.forms[0]);
								APP.fetch({
									method:'POST',
									data: formData,
									formData: true,
									url: 'handlers/addCompany.php',
									transport: 'json',
									callback(data) {
										if (data) {
											APP.fetch({
												method:'POST',
												data:data,
												url:'handlers/checkCompany.php',
												transport: 'json',
												callback(data) {
													obj['lab']['diler'] = data;
													btns.children[1].remove();

													let diler = document.createElement('span');
													diler.classList.add('btn-link');
													diler.innerText = obj['lab']['diler'].TITLE;
													diler.addEventListener('click', function () {
														BX24.openApplication({
															'action':'widget_url',
															'url':  'https://' + BX24.getDomain() + `/crm/company/details/${obj['lab']['diler'].ID}/?IFRAME=Y&IFRAME_TYPE=SIDE_SLIDER`
														});
													});
													btns.append(diler);
												}
											});

											APP.notice({
												content: `<p class="text-center h5 pt-2">ID ${data}. Создана новая компания.</p>`,
												type: 'm-success'
											});

										} else {
											APP.notice({
												content: '<p class="text-center h5 pt-2">Произошла ошибка. Компания не создалась.</p>',
												type: 'm-danger'
											});
										}
									}
								});
								document.body.removeEventListener('click', finder);
							}
						}));
					}
				});
			});
			parent.append(diler);
		}
	}
}

function createWindow (data) {
	let html = '<form>';
	for (let i in data) {
		if (['ID', 'EMAIL', 'CURRENCY_ID', 'HONORIFIC', 'COMPANY_IDS'].includes(i)) continue;
		if (data[i].type == 'string') {
			html += `
				<div class='form-group'>
					<label for='${i}'>${data[i].listLabel || data[i].title}</label>
					<input type='text' class='form-control' id='${i}' name='${i}'>
				</div>
			`;

		} else if (['integer', 'double'].includes(data[i].type)) {
			html += `
				<div class='form-group'>
					<label for='${i}'>${data[i].listLabel || data[i].title}</label>
					<input type='number' class='form-control' id='${i}' name='${i}'>
				</div>
			`;

		} else if (['date', 'datetime'].includes(data[i].type)) {
			html += `
				<div class='form-group'>
					<label for='${i}'>${data[i].listLabel || data[i].title}</label>
					<input type='date' class='form-control' id='${i}' name='${i}'>
				</div>
			`;

		} else if (['boolean', 'char'].includes(data[i].type)) {
			html += `
				<div class='form-group' style='width: 35%; display:flex; justify-content:space-between;'>
					<label style='display:block;' class='form-check-label ml-2' for='${i}'>${data[i].listLabel || data[i].title}</label>
					<input style='display: block;' type='checkbox' class='form-check-input' id='${i}' name='${i}'>
				</div>
			`;

		} else if (['user', 'employee'].includes(data[i].type)) {
			html += `
				<div class='form-group'>
					<label for='${i}'>${data[i].listLabel || data[i].title}</label>
					<select class='form-control' id='${i}' name='${i}'>
						<option value=''>Не установлен</option>
						${Object.keys(USERS).map(value => `<option value='${value}'>${USERS[value]}</option>`).join('')}
					</select> 
				</div>
			`;

		} else if (i == 'COMPANY_TYPE') {
			html += `
				<div class='form-group'>
					<label for='${i}'>${data[i].listLabel || data[i].title}</label>
					<select class='form-control' id='${i}' name='${i}'>
						<option value=''>Не установлен</option>
						<option value='CUSTOMER'>Клиент</option>
						<option value='SUPPLIER'>Поставщик</option>
						<option value='COMPETITOR'>Конкурент</option>
						<option value='PARTNER'>Партнер</option>
						<option value='OTHER'>Другое</option>
					</select>
				</div>
			`;

		} else if (['CONTACT_ID', 'LEAD_ID', 'COMPANY_ID'].includes(i)) {
			let group = document.createElement('div');
			group.classList.add('form-group');

			let label = document.createElement('label');
			label.for = i;
			label.innerText = data[i].title;

			let input = document.createElement('input');
			input.classList.add('form-control');
			input.type = 'text';
			input.id = i;
			input.name = i;
			input.dataset.finder = true;
			document.body.addEventListener('click', finder);

			group.append(label);
			group.append(input);
			html += group.innerHTML;

		} else if (i == 'EMPLOYEES') {
			html += `
				<div class='form-group'>
					<label for='${i}'>${data[i].title}</label>
					<select class='form-control' id='${i}' name='${i}'>
						<option value=''>Не установлен</option>
						<option value='EMPLOYEES_1'>менее 50</option>
						<option value='EMPLOYEES_2'>50-250</option>
						<option value='EMPLOYEES_3'>250-500</option>
						<option value='EMPLOYEES_4'>более 500</option>
					</select>
				</div>
			`;
		} else if (i == 'IM') {
			html += `
				<div class='form-group row'>
					<label class='col-12' for='${i}'>${data[i].title}</label>
					<input type='text' class='form-control col-6 ml-3' id='${i}' name='${i}'>
					<select class='form-control col-5' name='${i}_2'>
						<option value=''>Не установлен</option>
						<option value='FACEBOOK'>FACEBOOK</option>
						<option value='TELEGRAM'>TELEGRAM</option>
						<option value='VK'>VK</option>
						<option value='SKYPE'>SKYPE</option>
						<option value='VIBER'>VIBER</option>
						<option value='INSTAGRAM'>INSTAGRAM</option>
						<option value='BITRIX24'>BITRIX24</option>
						<option value='OPENLINE'>OPENLINE</option>
						<option value='IMOL'>IMOL</option>
						<option value='ICQ'>ICQ</option>
						<option value='MSN'>MSN</option>
						<option value='JABBER'>JABBER</option>
						<option value='OTHER'>OTHER</option>
					</select>
				</div>
			`;
			
		} else if (i == 'INDUSTRY') {
			html += `
				<div class='form-group'>
					<label for='${i}'>${data[i].title}</label>
					<select class='form-control' id='${i}' name='${i}'>
						<option value=''>Не установлен</option>
						<option value='IT'>Морфология</option>
						<option value='OTHER'>Микробиология</option>
						<option value='1'>Профили ИГХ</option>
						<option value='2'>Профили ЖЦ</option>
						<option value='4'>Профили МОРГ</option>
					</select>
				</div>
			`;
		} else if (data[i].type == 'file') {
			html += `
				<div class='form-group'>
					<label for='${i}'>${data[i].listLabel || data[i].title}</label>
					<input type='file' class='form-control' id='${i}' name='${i}'>
				</div>
			`;

		} else if (i == 'PHONE') {
			html += `
				<div class='form-group row'>
					<label class='col-12' for='${i}'>${data[i].title}</label>
					<input type='text' class='form-control col-6 ml-3' id='${i}' name='${i}'>
					<select class='form-control col-5' name='${i}_2'>
						<option value=''>Не установлен</option>
						<option value='WORK'>Рабочий</option>
						<option value='MOBILE'>Мобильный</option>
						<option value='FAX'>Факс</option>
						<option value='HOME'>Домашний</option>
						<option value='PAGER'>Пейджер</option>
						<option value='MAILING'>Для рассылок</option>
						<option value='OTHER'>Другой</option>
					</select>
				</div>
			`;

		} else if (data[i].type == 'enumeration') {
			html += `
				<div class='form-group'>
					<label for='${i}'>${data[i].listLabel || data[i].title}</label>
					<select class='form-control' id='${i}' name='${i}'>
						${data[i].items.map(value => `<option value='${value.ID}'>${value.VALUE}</option>`).join('')}
					</select>
				</div>
			`;

		} else if (i == 'WEB') {
			html += `
				<div class='form-group row'>
					<label class='col-12' for='${i}'>${data[i].title}</label>
					<input type='text' class='form-control col-6 ml-3' id='${i}' name='${i}'>
					<select class='form-control col-5' name='${i}_2'>
						<option value=''>Не установлен</option>
						<option value='WORK'>Корпоративный</option>
						<option value='HOME'>Личный</option>
						<option value='FACEBOOK'>FACEBOOK</option>
						<option value='VK'>VK</option>
						<option value='LIVEJOURNAL'>LIVEJOURNAL</option>
						<option value='TWITTER'>TWITTER</option>
						<option value='OTHER'>Другой</option>
					</select>
				</div>
			`;

		}
	}

	html += '</form>';
	return html;
}

function finder (e) {
	if (e.target.dataset.finder) {
		let type = e.target.id.split('_')[0].toLowerCase();
		BX24.selectCRM({entityType: [type]}, 
		function(){
			for (let i in arguments[0]) {
				if (arguments[0][i][0])
					e.target.value = arguments[0][i][0].title + '|' + arguments[0][i][0].id;
			}
		});
	}
}