<?php
/**
* scopes: crm, placement
*/
use libs\crest\CRest;
use libs\crest\CRestPlus;
use libs\debugger\Debugger;
define ('CLIENT', __DIR__.'/libs/crest/settings.json');
define ('INSTALL', __DIR__.'/libs/crest/install.php');
define ('HANDLER', 'https://script.dizlab.com/vadim/biovitrum/index.php');
spl_autoload_register(function ($class) {
	$path = __DIR__.'/'.str_replace('\\', '/', $class).'.php';
	if (file_exists($path)) require_once $path;
	else {
		require_once __DIR__.'/error.php';
		die();
	}
});

Debugger::displayErrors(1);
#===================================== settings ==============================#
### установка приложения
if (isset($_REQUEST['PLACEMENT']) && !file_exists(CLIENT)) {
	require_once INSTALL;
	CRestPlus::call('placement.bind', array(
		'PLACEMENT' => 'CRM_COMPANY_DETAIL_TAB',
		'HANDLER'   => HANDLER,
		// 'TITLE'     =>
	));
}

if(isset($_POST['PLACEMENT_OPTIONS']) && !empty($_POST['PLACEMENT_OPTIONS']))
	$PLACEMENT_OPTIONS = json_decode($_POST['PLACEMENT_OPTIONS'], 1);

$idCurrentCompany = json_decode($_POST['PLACEMENT_OPTIONS'], 1)['ID'] ?? '1';
$CURRENT_COMPANY = CRestPlus::call('crm.company.get', array('id' => $idCurrentCompany));

if (isset($PLACEMENT_OPTIONS['action']) && $PLACEMENT_OPTIONS['action'] == 'widget_button_url') {
	header('Location: '.$PLACEMENT_OPTIONS['url']);
	die();
} else if (isset($PLACEMENT_OPTIONS['action']) && $PLACEMENT_OPTIONS['action'] == 'widget_url') {
	header('Location: '.$PLACEMENT_OPTIONS['url']);
	die();
}

$USERS = array();
$getUsers = CRestPlus::callBatchList('user.get', array('FILTER' => array('ACTIVE' => 'Y')));
foreach ($getUsers as $users) {
	foreach ($users['result']['result'] as $value) {
		foreach ($value as $v)
			$USERS[$v['ID']] = $v['LAST_NAME'].' '.$v['NAME'];
	}
}