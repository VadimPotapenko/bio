<?php
use libs\crest\CRest;
use libs\crest\CRestPlus;
use libs\debugger\Debugger;
require_once __DIR__.'/settings.php';
#=========================== settings =============================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);

$checkCompany = CRestPlus::call('crm.contact.get', array('id' => $json_arr));
echo json_encode($checkCompany['result']);