<?php
spl_autoload_register(function ($class) {
	$path = dirname(__DIR__).'/'.str_replace('\\', '/', $class).'.php';
	if (file_exists($path)) require_once $path;
	else {
		require_once dirname(__DIR__).'/error.php';
		die();
	}
});
