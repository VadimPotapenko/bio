<?php
use libs\crest\CRest;
use libs\crest\CRestPlus;
use libs\debugger\Debugger;
require_once __DIR__.'/settings.php';
#=========================== settings =============================#
$newCompany = CRestPlus::call('crm.contact.add', array('fields' => $_POST));
echo json_encode($newCompany['result']);