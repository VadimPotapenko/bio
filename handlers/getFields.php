<?php
use libs\crest\CRest;
use libs\crest\CRestPlus;
use libs\debugger\Debugger;
require_once __DIR__.'/settings.php';
#=========================== settings =============================#
$json_str = file_get_contents('php://input');
$json_arr = json_decode($json_str, 1);

$method = 'crm.'.$json_arr.'.fields';
$fields = CRestPlus::call($method, array());
echo json_encode($fields['result']);