<?php
use libs\crest\CRest;
use libs\crest\CRestPlus;
use libs\debugger\Debugger;
require_once __DIR__.'/settings.php';
#=========================== settings =============================#

// $test = CRestPlus::call('crm.company.userfield.list', array('filter' => array('FIELD_NAME' => 'COMPANY_TYPE')));
$test = CRestPlus::call('crm.company.list', array('filter' => array('ID' => '1'), 'select' => array('WEB')));
// $test = CRestPlus::call('crm.multifield.fields', array());
Debugger::debug($test);
